import random
import sys

from cryptoy.utils import (
    pow_mod,
)

sys.setrecursionlimit(5000)  # Required for pow_mod for large exponents


def keygen(prime_number: int, generator: int) -> dict[str, int]:
    # 1. Tirer aléatoirement un nombre secret private_key entre 2 et prime_number - 1 inclus avec random.randint(min, max)
    private_key = random.randint(2, prime_number - 1)

    # 2. Calculer la clef publique public_key = generator ** private_key % prime_number en utilisant pow_mod
    public_key = pow_mod(generator, private_key, prime_number)

    # 3. Renvoyer le dictionnaire {"public_key": A, "private_key": a}
    return {"public_key": public_key, "private_key": private_key}


def compute_shared_secret_key(public: int, private: int, prime_number: int) -> int:
    # Implementer le calcul de la clef secrete partagée à partir de la clef publique de l'autre participant et de ma clef privée
    # Utiliser pow_mod
    shared_secret = pow_mod(public, private, prime_number)
    return shared_secret
